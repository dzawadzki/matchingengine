#pragma once

#include <OrderCache.h>

#include <unordered_map>
#include <map>
#include <unordered_set>
#include <string>
#include <iostream>
#include <mutex>

// this should be typically handled by proper logging library
#ifdef NDEBUG
    #define COUT std::cout
#else
    #define COUT NullStream()
#endif

class NullStream {
    public:
    NullStream() { }
    template<typename T> NullStream& operator<<(T const&) { return *this; }
    NullStream & operator<<(std::ostream & (*)(std::ostream&))
    {
      return *this;
    }
};

class OrderCache : public OrderCacheInterface
{
public:
    virtual void addOrder(Order order) override;

    virtual void cancelOrder(const std::string& orderId) override;

    virtual void cancelOrdersForUser(const std::string& user) override;

    virtual void cancelOrdersForSecIdWithMinimumQty(const std::string& securityId, unsigned int minQty) override;

    virtual unsigned int getMatchingSizeForSecurity(const std::string& securityId) override;

    virtual std::vector<Order> getAllOrders() const override;

    std::string render() const;

    // almost every type is a string
    // so adding these typedefs for type declaration readability
    using OrderId = std::string;
    using User = std::string;

    // orderId -> order
    using Orders = std::unordered_map<OrderId, Order>;

    // This could have been std::unordered_set<std::reference_wrapper<Order>>
    // but this would also require std::hash and equality operators to be defined
    // and the result would be no safer than plain old pointer.
    // Therefore I opted for a simpler solution (syntactically at least).
    using UserOrders = std::unordered_set<Order*>;
    using QtyOrders = std::unordered_set<Order*>;

    // user -> [*order]
    using Users = std::unordered_map<User, UserOrders>;
    using Security = std::string;
    using Quantity = unsigned int;
    using Quantities = std::map<Quantity, QtyOrders>;
    using Book = std::unordered_map<Security, Quantities>;

private:
    void removeOrderFromUser(Order* order_ptr);
    void removeOrderFromBook(Order* order_ptr);
    unsigned int matchOrders(QtyOrders::iterator& sell_order, QtyOrders::iterator& buy_order, bool& sell_discarded);
    Book& getBook(const Order& order);
    void removeOrder(const std::string& orderId);
    std::string render2(const std::string& securityId) const;

private:
    mutable std::mutex m_apiAccessLock;
    Orders m_orders;
    Users m_users;
    Book m_buy;
    Book m_sell;
};
