#include <OrderCacheImpl.h>

#include <sstream>
#include <iostream>
#include <algorithm>

void OrderCache::cancelOrder(const std::string &orderId)
{
    std::lock_guard _(m_apiAccessLock);
    removeOrder(orderId);
}


void OrderCache::removeOrder(const std::string &orderId)
{
    auto orders_iter = m_orders.find(orderId);
    if (orders_iter == m_orders.end())
    {
        // silently ignore invalid argument. Alternatively we could throw an exception here.
        return;
    }
    auto order_ptr = &orders_iter->second;

    removeOrderFromUser(order_ptr);
    removeOrderFromBook(order_ptr);

    // must always be last as it invalidates all Order pointers
    m_orders.erase(orders_iter);
}

void OrderCache::addOrder(Order order)
{
    std::lock_guard _(m_apiAccessLock);
    // method must return void so assuming exception based erorr handling is expected
    if (m_orders.count(order.orderId()))
    {
        // silently ignore invalid argument
        return;
    }

    auto it = m_orders.insert_or_assign(order.orderId(), order);
    auto order_ptr = &it.first->second;
    m_users[order.user()].insert(order_ptr);

    getBook(order)[order.securityId()][order.qty()].insert(order_ptr);
}

void OrderCache::cancelOrdersForUser(const std::string &user)
{
    std::lock_guard _(m_apiAccessLock);
    auto users_it = m_users.find(user);
    if (users_it == m_users.end())
    {
        // silently ignore invalid argument
        return;
    }

    for (auto el : users_it->second)
    {
        removeOrderFromBook(el);

        // must always be last as it invalidates Order pointers
        m_orders.erase(el->orderId());
    }
    m_users.erase(user);
}

void OrderCache::cancelOrdersForSecIdWithMinimumQty(const std::string &securityId, unsigned int minQty)
{
    std::lock_guard _(m_apiAccessLock);
    auto rem = [this](auto book, auto securityId, auto minQty)
    {
        // micro optimization: cache book[sec_id]::end()
        auto end = book[securityId].end();

        for(auto it = book[securityId].lower_bound(minQty); it != end; ++it)
        {
            for (auto order_ptr : it->second)
            {
                removeOrder(order_ptr->orderId());
            }
        }
    };

    rem(m_buy, securityId, minQty);
    rem(m_sell, securityId, minQty);
}

unsigned int OrderCache::matchOrders(QtyOrders::iterator &sell_order, QtyOrders::iterator& buy_order, bool& sell_discarded)
{
    COUT << "matching S: " << (*sell_order)->orderId() << ", B: " << (*buy_order)->orderId();

    auto traded_qty = std::min((*buy_order)->qty(), (*sell_order)->qty());
    COUT << ", matched qty: " << traded_qty << std::endl;

    auto update_order = [this, traded_qty](auto& order_it) -> bool
    {
        auto order_qty = (*order_it)->qty();
        if (order_qty == traded_qty)
        {
            COUT << "discard order " << (*order_it)->orderId() << std::endl;
            auto inc_it = order_it;
            ++inc_it;
            removeOrder((*order_it)->orderId());
            order_it = inc_it;
            return true;
        }
        (*order_it)->updateQty(order_qty - traded_qty);

        // reposition element accordingly to new qty value
        auto& book = getBook(**order_it);
        auto& qty_ref = book[(*order_it)->securityId()];
        auto new_qty = (*order_it)->qty();
        auto node = qty_ref[order_qty].extract(*order_it);
        order_it = qty_ref[new_qty].insert(std::move(node)).position;
        return false;
    };

    update_order(buy_order);
    sell_discarded = update_order(sell_order);

    return traded_qty;
}

unsigned int OrderCache::getMatchingSizeForSecurity(const std::string &securityId)
{
    std::lock_guard _(m_apiAccessLock);
    unsigned int traded_qty = 0;

    if (!m_sell.count(securityId) || !m_buy.count(securityId))
    {
        return 0;
    }

    // To get rid of this nested loop a custom iterator could be defined.
    // The overall code complication would however increase.
    // This can be alleviated by using boost/iterator/iterator_facade.hpp
    // which is not viable for this project.
    auto sell_qty_end_it = m_sell.at(securityId).end();
    for (auto sell_qty_it = m_sell.at(securityId).begin(); sell_qty_it != sell_qty_end_it; ++sell_qty_it)
    {
        for (QtyOrders::iterator sell_order_it = sell_qty_it->second.begin(); sell_order_it != sell_qty_it->second.end();)
        {
            //=============================================================================
            auto buy_qty_end_it = m_buy.at(securityId).end();
            bool sell_discarded = false;
            auto next_sell_it = sell_order_it;
            ++next_sell_it;
            for (auto buy_qty_it = m_buy.at(securityId).begin(); !sell_discarded && buy_qty_it != buy_qty_end_it; ++buy_qty_it)
            {
                for (QtyOrders::iterator buy_order_it = buy_qty_it->second.begin(); !sell_discarded && buy_order_it != buy_qty_it->second.end();)
                {
                    auto next_buy_it = buy_order_it;
                    ++next_buy_it;
                    COUT << (*sell_order_it)->orderId() << " -> ";
                    COUT << (*buy_order_it)->orderId() << std::endl;
                    COUT << render2(securityId);
                    COUT << "----------------------------\n";
                    if ((*sell_order_it)->company() != (*buy_order_it)->company())
                    {
                        // match orders updates both iterators
                        traded_qty += matchOrders(sell_order_it, buy_order_it, sell_discarded);
                    }
                    COUT << render2(securityId);
                    COUT << "\n\n";
                    buy_order_it = next_buy_it;
                }
            }
            //=============================================================================
            sell_order_it = next_sell_it;
        }
    }

    return traded_qty;
}

// I expect this method is used for testing purposes
// so O(1) complexity is not required here.
// Current complexity: O(n)
std::vector<Order> OrderCache::getAllOrders() const
{
    std::vector<Order> retval;

    std::lock_guard _(m_apiAccessLock);

    retval.reserve(m_orders.size());

    for(auto& elem : m_orders)
    {
        retval.push_back(elem.second);
    }

    return retval;
}

std::string OrderCache::render() const
{
    std::stringstream ss;

    std::lock_guard _(m_apiAccessLock);

    for(auto& o : m_orders)
    {
        ss << o.second.orderId() << " " << o.second.securityId() << " " << o.second.side() << " "
            << o.second.qty() << " " << o.second.user() << " " << o.second.company() << std::endl;
    }

    return ss.str();
}

std::string OrderCache::render2(const std::string& securityId) const
{
    std::stringstream ss;

    if (m_sell.count(securityId))
    {
        ss << "SELL: \n";
        for (auto sell_qty_it = m_sell.at(securityId).begin(); sell_qty_it != m_sell.at(securityId).end(); )
        {
            auto sell_qty_cur = sell_qty_it++;
            ss << "    qty: " << sell_qty_cur->first << std::endl;
            for (auto sell_order_it = sell_qty_cur->second.begin(); sell_order_it != sell_qty_cur->second.end();)
            {
                auto sell_order_cur = sell_order_it++;
                ss << "        order id:" << (*sell_order_cur)->orderId() << std::endl;
            }
        }
    }

    if (m_buy.count(securityId))
    {
        ss << "BUY:" << std::endl;
        for (auto buy_qty_it = m_buy.at(securityId).begin(); buy_qty_it != m_buy.at(securityId).end(); )
        {
            auto buy_qty_cur = buy_qty_it++;
            ss << "    qty: " << buy_qty_cur->first << std::endl;
            for (auto buy_order_it = buy_qty_cur->second.begin(); buy_order_it != buy_qty_cur->second.end();)
            {
                auto buy_order_cur = buy_order_it++;
                ss << "        order id:" << (*buy_order_cur)->orderId() << std::endl;
            }
        }
    }

    return ss.str();
}

void OrderCache::removeOrderFromUser(Order *order_ptr)
{
    auto user_name = order_ptr->user();
    auto users_iter = m_users.find(user_name);

    // assuming users_iter must be valid, otherwise structure is corrupt
    users_iter->second.erase(order_ptr);

    // Removing empty user cache entry. Not strictly necessary
    // but in some cases not doing it may incur runtime memory overhead.
    // Implemented for the sake of completness.
    if (users_iter->second.empty())
    {
        m_users.erase(users_iter);
    }
}

void OrderCache::removeOrderFromBook(Order *order_ptr)
{
    auto &book = getBook(*order_ptr);
    auto &sec = book[order_ptr->securityId()];
    auto &orders = sec[order_ptr->qty()];
    orders.erase(order_ptr);
}

OrderCache::Book &OrderCache::getBook(const Order &order)
{
    if (order.side() == "Buy")
        return m_buy;
    return m_sell;
}
