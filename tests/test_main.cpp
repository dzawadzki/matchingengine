#define CATCH_CONFIG_MAIN
#include "frameworks/catch/catch.hpp"

#include <OrderCacheImpl.h>

#include <string>
#include <iostream>
#include <sstream>

using namespace std;

namespace {
const string test_input_1{R"(
            OrdId1 SecId1 Sell 100 User10 Company2
            OrdId2 SecId3 Sell 200 User8 Company2
            OrdId3 SecId1 Buy 300 User13 Company2
            OrdId5 SecId3 Sell 500 User7 Company2
            OrdId6 SecId3 Buy 600 User3 Company1
            OrdId7 SecId1 Sell 700 User10 Company2
            OrdId8 SecId1 Sell 800 User2 Company1
            OrdId13 SecId1 Sell 1300 User1 Company
            OrdId11 SecId1 Sell 1100 User13 Company2
            OrdId10 SecId2 Sell 1000 User5 Company1
            OrdId4 SecId2 Sell 400 User12 Company2
            OrdId9 SecId2 Buy 900 User6 Company2
            OrdId12 SecId2 Buy 1200 User9 Company2
)"};


const string test_input_2{R"(
            OrdId1 SecId3 Sell 100 User1 Company1
            OrdId2 SecId3 Sell 200 User3 Company2
            OrdId3 SecId1 Buy 300 User2 Company1
            OrdId4 SecId3 Sell 400 User5 Company2
            OrdId5 SecId2 Sell 500 User2 Company1
            OrdId6 SecId2 Buy 600 User3 Company2
            OrdId7 SecId2 Sell 700 User1 Company1
            OrdId8 SecId1 Sell 800 User2 Company1
            OrdId9 SecId1 Buy 900 User5 Company2
            OrdId10 SecId1 Sell 1000 User1 Company1
            OrdId11 SecId2 Sell 1100 User6 Company2
)"};

const string test_input_3{R"(
            OrdId1 SecId1 Buy  1000 User1 CompanyA
            OrdId2 SecId2 Sell 3000 User2 CompanyB
            OrdId3 SecId1 Sell  500 User3 CompanyA
            OrdId4 SecId2 Buy   600 User4 CompanyC
            OrdId5 SecId2 Buy   100 User5 CompanyB
            OrdId6 SecId3 Buy  1000 User6 CompanyD
            OrdId7 SecId2 Buy  2000 User7 CompanyE
            OrdId8 SecId2 Sell 5000 User8 CompanyE
)"};
}

class TestInputParser
{
public:
    TestInputParser(OrderCache& oc)
        : m_oc{oc}
    {

    }

    void addOrders(const string& str)
    {
        stringstream ss(str);
        string line;
        while (getline(ss, line))
        {
            if (line.size())
            {
                stringstream parser(line);

                std::string id, sec, side, user, company;
                unsigned int qty = 0;
                parser >> id >> sec >> side >> qty >> user >> company;
                if (!parser.fail())
                {
                    Order order(id, sec, side, qty, user, company);
                    m_oc.addOrder(order);
                }
            }
        }

    }

private:
    OrderCache& m_oc;
};

// custom matcher to verify if cache contains order with specific id
class ContainOrderWithId : public Catch::MatcherBase<const vector<Order>&>
{
public:
    ContainOrderWithId(string id) : m_id(id)
    {}

    bool match(const vector<Order>& in) const override
    {
        for (auto i : in)
        {
            if (i.orderId() == m_id)
            {
                return true;
            }
        }
        return false;
    }

    std::string describe() const override
    {
        std::ostringstream ss;
        ss << "contain order with id " << m_id;
        return ss.str();
    }
private:
    const string m_id;
};


TEST_CASE("insertion deletion tests", "[addinsert]")
{
    OrderCache oc;
    TestInputParser ip(oc);

    SECTION("test simple add remove")
    {
        ip.addOrders("OrdId1 SecId3 Sell 100 User1 Company1");
        auto orders = oc.getAllOrders();
        REQUIRE(orders.size() == 1);
        CHECK(orders[0].orderId() == "OrdId1");
        oc.cancelOrder("OrdId1");
        CHECK(oc.getAllOrders().size() == 0);
    }

    SECTION("test multiple add remove")
    {
        ip.addOrders(test_input_1);
        REQUIRE(oc.getAllOrders().size() == 13);
        oc.cancelOrder("OrdId13");
        CHECK(oc.getAllOrders().size() == 12);
        oc.cancelOrder("OrdId12");
        CHECK(oc.getAllOrders().size() == 11);

        // cancel already cancelled, should be no-op
        oc.cancelOrder("OrdId13");
        CHECK(oc.getAllOrders().size() == 11);
    }

    SECTION("test cancel order for user")
    {
        ip.addOrders(test_input_2);
        REQUIRE(oc.getAllOrders().size() == 11);
        oc.cancelOrdersForUser("User2");
        CHECK(oc.getAllOrders().size() == 8);
        REQUIRE_THAT(oc.getAllOrders(), !ContainOrderWithId("OrdId3"));
        REQUIRE_THAT(oc.getAllOrders(), !ContainOrderWithId("OrdId5"));
        REQUIRE_THAT(oc.getAllOrders(), !ContainOrderWithId("OrdId8"));

        oc.cancelOrdersForUser("User2");
        CHECK(oc.getAllOrders().size() == 8);

        oc.cancelOrdersForUser("User1");
        CHECK(oc.getAllOrders().size() == 5);
        REQUIRE_THAT(oc.getAllOrders(), !ContainOrderWithId("OrdId1"));
        REQUIRE_THAT(oc.getAllOrders(), !ContainOrderWithId("OrdId7"));
        REQUIRE_THAT(oc.getAllOrders(), !ContainOrderWithId("OrdId10"));
    }

    SECTION("test remove by security id with minimym qty")
    {
        ip.addOrders(test_input_1);
        REQUIRE_THAT(oc.getAllOrders(), ContainOrderWithId("OrdId8"));
        REQUIRE_THAT(oc.getAllOrders(), ContainOrderWithId("OrdId11"));
        REQUIRE_THAT(oc.getAllOrders(), ContainOrderWithId("OrdId13"));
        oc.cancelOrdersForSecIdWithMinimumQty("SecId1", 800);
        REQUIRE_THAT(oc.getAllOrders(), !ContainOrderWithId("OrdId8"));
        REQUIRE_THAT(oc.getAllOrders(), !ContainOrderWithId("OrdId11"));
        REQUIRE_THAT(oc.getAllOrders(), !ContainOrderWithId("OrdId13"));
    }

    SECTION("test adding order with duplicate id")
    {
        ip.addOrders("OrdId1 SecId1 Sell 100 User1 Company1");
        REQUIRE_THAT(oc.getAllOrders(), ContainOrderWithId("OrdId1"));
        ip.addOrders("OrdId1 SecId2 Sell 10 User2 Company2");
        // order should not be replaced or updated
        CHECK(oc.getAllOrders()[0].securityId() == "SecId1");
    }
}

TEST_CASE("matching tests", "[matching]")
{
    OrderCache oc;
    TestInputParser ip(oc);

    SECTION("test matching 0")
    {
        ip.addOrders("OrdId1 SecId1 Sell 1000 User1 Company1");
        ip.addOrders("OrdId2 SecId1 Sell 200 User1 Company1");
        ip.addOrders("OrdId3 SecId1 Buy 1000 User1 Company2");
        ip.addOrders("OrdId4 SecId1 Buy 1200 User1 Company2");
        CHECK(oc.getMatchingSizeForSecurity("SecId1") == 1200);
    }
    
    SECTION("test matching 1")
    {
        ip.addOrders(test_input_1);
        CHECK(oc.getMatchingSizeForSecurity("SecId1") == 300);
        CHECK(oc.getMatchingSizeForSecurity("SecId2") == 1000);
        CHECK(oc.getMatchingSizeForSecurity("SecId3") == 600);
    }
    
    SECTION("test matching 2")
    {
        ip.addOrders(test_input_2);
        CHECK(oc.getMatchingSizeForSecurity("SecId1") == 900);
        CHECK(oc.getMatchingSizeForSecurity("SecId2") == 600);
        CHECK(oc.getMatchingSizeForSecurity("SecId3") == 0);
    }

    SECTION("test matching 3")
    {
        ip.addOrders(test_input_3);
        CHECK(oc.getMatchingSizeForSecurity("SecId1") == 0);
        CHECK(oc.getMatchingSizeForSecurity("SecId2") == 2700);
        CHECK(oc.getMatchingSizeForSecurity("SecId3") == 0);
    }

    SECTION("test matching 4")
    {
        ip.addOrders("OrdId1 SecId1 Sell 100 User1 Company1");
        ip.addOrders("OrdId2 SecId1 Buy 100 User1 Company2");
        ip.addOrders("OrdId3 SecId1 Buy 100 User1 Company3");
        ip.addOrders("OrdId4 SecId1 Buy 100 User1 Company4");
        CHECK(oc.getMatchingSizeForSecurity("SecId1") == 100);
    }

    SECTION("test matching 5")
    {
        ip.addOrders("OrdId1 SecId1 Sell 100 User1 Company1");
        ip.addOrders("OrdId2 SecId1 Sell 100 User1 Company1");
        ip.addOrders("OrdId9 SecId1 Buy 100 User1 Company2");
        CHECK(oc.getMatchingSizeForSecurity("SecId1") == 100);
        ip.addOrders("OrdId9 SecId1 Buy 100 User1 Company2");
        CHECK(oc.getMatchingSizeForSecurity("SecId1") == 100);
    }

    SECTION("test matching 6")
    {
        ip.addOrders("OrdId1 SecId1 Sell 100 User1 Company1");
        ip.addOrders("OrdId2 SecId1 Sell 100 User1 Company1");
        ip.addOrders("OrdId3 SecId1 Sell 100 User1 Company1");
        ip.addOrders("OrdId4 SecId1 Buy 10 User1 Company2");
        ip.addOrders("OrdId5 SecId1 Buy 10 User1 Company2");
        ip.addOrders("OrdId6 SecId1 Buy 10 User1 Company2");
        CHECK(oc.getMatchingSizeForSecurity("SecId1") == 30);
        ip.addOrders("OrdId7 SecId1 Buy 100 User1 Company2");
        CHECK(oc.getMatchingSizeForSecurity("SecId1") == 100);
        ip.addOrders("OrdId8 SecId1 Buy 100 User1 Company2");
        ip.addOrders("OrdId9 SecId1 Buy 90 User1 Company2");
        ip.addOrders("OrdId10 SecId1 Buy 20 User1 Company2");
        CHECK(oc.getMatchingSizeForSecurity("SecId1") == 170);
        ip.addOrders("OrdId11 SecId1 Sell 40 User1 Company1");
        CHECK(oc.getMatchingSizeForSecurity("SecId1") == 40);
        CHECK(oc.getAllOrders().size() == 0);
    }
}
